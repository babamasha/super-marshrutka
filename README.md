```
void add_babka(marshrutka_t* bus) {
    struct babka *Katya = calloc(1, sizeof(struct babka));
    struct client_t *client = (struct client_t*)Katya;

    Katya->client.bus = bus;
    insert_client(client);

    Katya->hp = 100;
    strcpy(Katya->client.role, "babka");
    bus->n_babok++;
}

void delete_babka(struct babka* Katya) {
    struct client_t *Klava = (struct client_t*)Katya;
    Klava->bus->n_babok--;
    delete_client(Klava);
}
```
